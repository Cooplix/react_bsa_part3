import env from '../env';

export const {secret} = env.app;
export const expires = '12h';