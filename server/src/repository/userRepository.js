import fs from 'fs';
import path from 'path'
import { v4 as uuid } from 'uuid';

export function findUserByLogin(login) {
    const userJson = fs.readFileSync(path.resolve(__dirname, '../data/user.json'));
    const users = JSON.parse(userJson);
    const user = users.find(userArray => userArray.login === login);
    return (user) || undefined;
}

export async function addUser(data) {
    const newUser ={
        id: uuid(),
        name: data.username,
        password: data.password,
        login: data.login,
        avatar: '',
    };
}