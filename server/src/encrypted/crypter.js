import bcrypt from 'bcrypt';


const rounds = 10;

export const encrypt = data => bcrypt.hash(data, rounds);