import dotenv from 'dotenv';

dotenv.config();

const env = {
    app: {
        port: process.env.APP_PORT,
        socketPort: process.env.SOCKET_PORT,
        secret: process.env.SECRET_KEY,
    },
};

export default env;