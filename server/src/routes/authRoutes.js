import { Router } from 'express';
import authentication from "../authentication/authentication";
import * as authService from '../authentication/authService';
import registration from "../registration/registration";


const router = Router();

router
    .post('/login', authentication, (req, res, next) => authService.login(req.user)
        .then(data => res.send(data))
        .catch(next))

    .post('/registration', registration, (req, res, next) => authService.registrationUser(req.user)
        .then(data => res.send(data))
        .catch(next))


export default router;