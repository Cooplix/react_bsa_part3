import jwt from 'jsonwebtoken';
import { secret, expires } from '../config/jwtConfig';
import { findUserByLogin, addUser } from '../repository/userRepository';
import { encrypt } from "../encrypted/crypter";

export const login = async ({ id }) => ({
    token: createToken({ id }),
    user: await findUserByLogin(id),
});

export const register = async ({ password, ...userData }) => {
    const newUser = await addUser({
        ...userData,
        password: await encrypt(password),
    });
    return login(newUser);
};


const createToken = data => jwt.sign( data, secret, { expires } );