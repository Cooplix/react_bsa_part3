import passport from 'passport';

export default passport.authenticate('registrationUser', {session: false});