import React from 'react'
import PropTypes from 'prop-types';
import {Form, Button, Segment} from 'semantic-ui-react';

const LoginForm = ({
        login,
        password,
        loginButtonLoading,
        setLoginAction,
        setPasswordAction,
        loginAction
    }) => {
    return (
        <Form name="loginForm" size="large" >
            <Segment>
                <Form.Input
                    fluid
                    icon="triangle right"
                    iconPosition="left"
                    placeholder="Login"
                    type="text"
                    value={login}
                    onChange={ev => {setLoginAction(ev.target.value)}}
                />
                <Form.Input
                    fluid
                    icon="lock"
                    iconPosition="left"
                    placeholder="Password"
                    type="password"
                    value={password}
                    onChange={ev => {setPasswordAction(ev.target.value)}}
                />
                <Button type="button" onClick={()=>loginAction()}loading={loginButtonLoading} disabled={loginButtonLoading} color="teal" fluid size="large"  primary>
                    Login
                </Button>
            </Segment>
        </Form>
    );
};

LoginForm.propTypes = {
    login:PropTypes.string.isRequired,
    password:PropTypes.string.isRequired,
    loginButtonLoading:PropTypes.bool.isRequired,
    setLoginAction: PropTypes.func.isRequired,
    setPasswordAction:PropTypes.func.isRequired,
    loginAction:PropTypes.func.isRequired,

};

export default LoginForm;
