import {
    SET_LOGIN_VALUE,
    SET_PASSWORD_VALUE,
    SET_LOGIN_LOADING_TOGGLE,
    CALL_LOGIN,
    LOGIN
} from "./actionTypes";

export const setLoginAction = (login) => ({
    type: SET_LOGIN_VALUE,
    login,
});

export const setPasswordAction = (password) => ({
    type: SET_PASSWORD_VALUE,
    password,
});

export const setLoginLoadingToggleAction = (loginBtnLoading) => ({
    type: SET_LOGIN_LOADING_TOGGLE,
    loginBtnLoading,
});

export const setLoginDataAction = (loginData) => ({
    type: CALL_LOGIN,
    loginData
});

export const loginAction = (loginData) => ({
    type: LOGIN,
});
