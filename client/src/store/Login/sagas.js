import { takeEvery, call, put, select } from "redux-saga/effects";
import {setLoginLoadingToggleAction, setLoginDataAction} from './action';
import { LOGIN } from './actionTypes';
import { loginApiCall } from "../../services/authService";

function* loginCall() {
    yield put(setLoginLoadingToggleAction(true));

    const { profile } = yield select()

    const login = profile.login;
    const password = profile.password;
    try {
        const userAuthData = yield call(loginApiCall,{login, password});
        console.log(userAuthData);
        const token = userAuthData.data.token;
        localStorage.setItem('token', token);
        yield put(setLoginDataAction(userAuthData))
    }
    catch(err) {
        console.log(err);
    }

    yield put(setLoginLoadingToggleAction(false));
}

export default function* watchLogin() {
    yield takeEvery(LOGIN, loginCall)
}
