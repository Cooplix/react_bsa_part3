import {
    SET_REGISTRATION_LOGIN_VALUE,
    SET_REGISTRATION_USERNAME_VALUE,
    SET_REGISTRATION_PASSWORD_VALUE,
    SET_REGISTRATION_LOADING_TOGGLE,
    CALL_REGISTRATION
} from './actionTypes';

const initialState = {
    login:"",
    username:"",
    password:"",
    user:{},
    isAuthorized:false,
    registrationBtnLoading:false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_REGISTRATION_LOGIN_VALUE:
            return {
                ...state,
                login: action.registrationBtnLoading,
            };
        case SET_REGISTRATION_USERNAME_VALUE:
            return {
                ...state,
                user: action.registrationUsername,
            };
        case SET_REGISTRATION_PASSWORD_VALUE:
            return {
                ...state,
                password: action.registrationPassword,
            };
        case SET_REGISTRATION_LOADING_TOGGLE:
            return {
                ...state,
                registrationBtnLoading: action.registrationBtnLoading,
            };
        default:
            return state;
    }
};
