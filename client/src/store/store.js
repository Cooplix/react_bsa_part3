import {applyMiddleware, createStore, compose, combineReducers} from "redux";
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
import {connectRouter, routerMiddleware} from 'connected-react-router';
import rootReducer from "../redux/reducers";
import {composeWithDevTools} from "redux-devtools-extension";
import createSagaMiddleware from 'redux-saga';


export const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();


const initialState = {};

const middlewares = [thunk, routerMiddleware(history), sagaMiddleware];

const composed = compose(
    composeWithDevTools(applyMiddleware(...middlewares))
);

const reducer = {
    messages: chatReducer,
    profile: profileReducer,
    registration: registrationReducer
};

const rootReducer = combineReducers({
    router: connectRouter(history),
    ...reducer,
});

const store = createStore(rootReducer, initialState, composed);

sagaMiddleware.run(rootSaga);

export default store;